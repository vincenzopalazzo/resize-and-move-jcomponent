plugins {
    // Apply the application plugin to add support for building a CLI application in Java.
    application
}

repositories {
    // Use JCenter for resolving dependencies.
    jcenter()
}

dependencies {
    implementation("io.github.vincenzopalazzo:material-ui-swing:1.1.2")
    implementation("io.github.material-ui-swing:DarkStackOverflowTheme:0.0.1-rc3")

    // Use JUnit Jupiter API for testing.
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.2")
    // Use JUnit Jupiter Engine for testing.
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")

}

application {
    // Define the main class for the application.
    mainClass.set("io.vincenzopalazzo.jresize.App")
}

tasks.test {
    // Use junit platform for unit tests.
    useJUnitPlatform()
}
