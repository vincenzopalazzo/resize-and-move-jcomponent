package io.vincenzopalazzo.jresize;

import io.vincenzopalazzo.jresize.ui.NewMaterialRootPaneUI;
import mdlaf.MaterialLookAndFeel;
import mdlaf.themes.MaterialTheme;

import javax.swing.*;

public class CustomMaterialLookAndFeel extends MaterialLookAndFeel {

    public CustomMaterialLookAndFeel(MaterialTheme theme) {
        super(theme);
    }

    @Override
    protected void initClassDefaults(UIDefaults table) {
        super.initClassDefaults(table);
        table.put("RootPaneUI", NewMaterialRootPaneUI.class.getCanonicalName());
    }
}
